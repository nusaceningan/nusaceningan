import { existsSync, unlinkSync } from "fs";
import { readFile, writeFile } from "fs/promises";
import path from "path";

const dir = "./dist/",
  oldfile1 = "sitemap-index.xml",
  oldfile2 = "sitemap-0.xml",
  outFile = "sitemap.xml";

const file = 
  await readFile(
    path.join(dir, oldfile2),
    "utf-8"
  ),
  xmlData = 
    file.replace(
      "<loc>https://nusaceningan.io/</loc>", 
      "<loc>https://nusaceningan.io</loc>"
    );

try{
  const securityFile = writeFile(
    path.join(dir, outFile), xmlData
  );

  const files = [
    path.join(dir, oldfile1), 
    path.join(dir, oldfile2)
  ];
  
  files.forEach(path => existsSync(path) && unlinkSync(path));

  console.log(
    `\x1b[33m@postbuild: \x1b[91m\`${
      oldfile1
    }\` \x1b[0m& \x1b[91m\`${
      oldfile2
    }\` \x1b[0mchanged into \x1b[32m\`${
      outFile
    }\`\x1b[0m☺
    `
  );

  }catch(err){
  console.log(err);
};