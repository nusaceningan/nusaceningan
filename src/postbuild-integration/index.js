import { readFile } from "fs/promises";
import { globby } from "globby";
import { parseHTML } from "linkedom";
import addPreloadStylesheet from "./utils/addPreloadStylesheet.js";
import removeFormatAttributes from "./utils/removeFormatAttributes.js";

/** @type {typeof import('astro').AstroIntegration} */
const postbuildIntegration = {
  name: "nusaceningan.io Custom Integration",
  hooks: {
    "astro:build:done": async () => {
      const allHtmlFilePaths = await globby("./dist/**/*.html");

      await Promise.all(
        allHtmlFilePaths
          .map(async (path) => {
            const htmlContent = await readFile(path, "utf8"),
              { document } = await parseHTML(htmlContent);

            return [
              removeFormatAttributes(path, document),
              addPreloadStylesheet(path, document),
            ];
          })
          .flat()
      );

      await Promise.all([
        import("./utils/generateRobotsMeta.js"),
        import("./utils/generateSecurityMeta.js"),
        import("./utils/generateXMLSitemap.js"),
        import("./utils/removeUnnecessaryFolder.js"),
      ]);
    },
  },
};

export default postbuildIntegration;
