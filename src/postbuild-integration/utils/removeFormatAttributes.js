import { writeFile } from "fs/promises";

export default async function addPreloadAttributes(path, document) {
  const formats = document.querySelectorAll("img[format]");

  [...formats].forEach((img) => {
    img.removeAttribute("format");
  });

  await writeFile(path, document.toString());
}
