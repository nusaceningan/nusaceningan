import { defineConfig } from "astro/config";
import tailwind from "@astrojs/tailwind";
import sitemap from "@astrojs/sitemap";
import image from "@astrojs/image";

import googleMapsData from "./src/utils/googleMapsData.js";
import imageFetching from "./src/utils/imageFetching.ts";
import postbuildIntegration from "./src/postbuild-integration/index.js";

// https://astro.build/config
export default defineConfig({
  site: "https://nusaceningan.io",

  vite: {
    plugins: [
      {
        name: "data-fetching",
        apply: () => { googleMapsData },
      },
      {
        name: "image-fetching",
        apply: () => { imageFetching },
      },
    ],
  },

  integrations: [
    tailwind(),
    image({
      serviceEntryPoint: "@astrojs/image/sharp"
    }),
    sitemap({
      i18n: {
        defaultLocale: "en",
        locales: {
          en: "en-US"
        }
      },
      lastmod: new Date()
    }),
    postbuildIntegration,
  ]
});