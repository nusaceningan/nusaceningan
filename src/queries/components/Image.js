const Image = `
  {
    data {
      attributes {
        url
        hash
        ext
        width
        height
        alternativeText
      }
    }
  }
`;

export default Image;
