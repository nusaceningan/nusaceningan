const Meta = `
  SEO {
    html_title
    metaDescription
    noindex
    nofollow
    URL_slug
    Canonical_link
  }
`;

export default Meta;
