import { writeFile } from "fs/promises";

export default async function addPreloadStylesheet(path, document) {
  const linkRel = document.querySelectorAll("link[rel='stylesheet']");

  [...linkRel].forEach((link) => {
    const href = link.getAttribute("href");

    var preloaderLink = document.createElement("link");
      preloaderLink.rel = "preload";
      preloaderLink.href = href;
      preloaderLink.setAttribute("as", "style");
    document.head.appendChild(preloaderLink);
  });

  await writeFile(path, document.toString());
}
