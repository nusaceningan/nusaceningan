import Image from "./components/Image.js";
import Meta from "./components/Meta.js";

const HomepageQuery = `
  {
    homepage {
      data {
        attributes {
          Title
          Title_tag
          Intro_text
          Intro_button_text
          Intro_button_link
          Intro_blob ${Image}
          Intro_blob_place_text
          Todo_title
          Todo_intro_txt
          Todo_intro_button_text
          Todo_intro_button_link
          Todo_button_text_1
          Todo_button_text_2
          Todo_button_text_3
          Todos {
            data {
              attributes {
                Title
                Description
                Intro_blob ${Image}
                ${Meta}
              }
            }
          }
          Accomodation_title
          Accomodation_button_text
          Accomodation_button_link
          Accommodations {
            data {
              attributes {
                Title
                Intro_blob ${Image}               
                ${Meta}
              }
            }
          }
          Boat_title
          Boat_intro_text
          Boat_intro_button_text
          Boat_intro_button_link
          Boats {
            data {
              attributes {
                Title
                Logo ${Image}
              }
            }
          }
          Instagram_title
          Instagram_button_text
          Instagram_images {
            data {
              attributes {
                Image_caption
                Image_link
                Image ${Image}
              }
            }
          }
          Geo_latitude
          Geo_longitude
          About_section_visibility
          About_section_order
          Todo_section_visibility
          Todo_section_order
          Accommodation_section_visibility
          Accommodation_section_order
          Review_section_visibility
          Review_section_order
          Boat_section_visibility
          Boat_section_order
          Instagram_section_visibility
          Instagram_section_oder
          ${Meta}
        }
      }
    }
  }
`;

export default HomepageQuery;
